//
//  FSlotsApiClient.h
//  CastleSlot
//
//  Created by Pavel Wasilenko on 03.05.17.
//  Copyright © 2017 Bars-i-Lis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CastleSlotApiClient : NSObject

- (NSString *)sendRequest;

@end
